const webHampik = (function () {
    const data = {
        inputDato: document.querySelectorAll('.input-dato'),
        btnLogin: document.querySelector('.container-form .container-button button'),
        btnSignUp: document.querySelector('.container-form .container-button button')
        // warningForm: document.getElementById('txt-warning')
    };

    const events = {
        onClickSignUp: function () {
            data.btnSignUp.addEventListener('click', (e) => {
                e.preventDefault();
                const email = document.getElementById('reg-email').value
                const nombre = document.getElementById('reg-fullName').value
                const celu = document.getElementById('reg-cellphone').value

                if (email && contra) {
                    document.getElementById('txt-warning').classList.remove('show')
                    methods.insertCliente(nombre, email, celu, contra)
                }
            })
        },
        onClickSent: function () {
            data.btnSignUp.addEventListener('click', (e) => {
                e.preventDefault();
                //   debugger

                const email = document.getElementById('reg-email').value
                const nombre = document.getElementById('reg-fullName').value
                const celu = document.getElementById('reg-cellphone').value

                if (methods.validateInputsMessage()) {
                    document.getElementById('txt-warning').classList.remove('show')
                    methods.validateLogin(email, nombre, celu)
                } else {
                    data.inputDato.forEach((e) => {
                        events.onTypeInput(e);
                    })
                }
            })
        }

    };

    const methods = {
        validateActiveButton: function () {
            // let count = 0
            // const cantInputs = data.inputDato.length
            // data.inputDato.forEach((e) => {
            //   e.value != '' ? count++ : true
            // })
            // count == cantInputs ? data.btnLogin.classList.add('active') : data.btnLogin.classList.remove('active')
        },

        validateInputsMessage: function () {
            const nombre = document.getElementById('reg-fullName')
            const email = document.getElementById('reg-email')
            const cel = document.getElementById('reg-cellphone')

            const patternEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;
            const patternCel = /^\w{2,3}$/;
            let validate = true

            if (nombre.value.length == 0) {
                nombre.parentElement.classList.add('show-warning')
                validate = false
            } else {
                nombre.parentElement.classList.remove('show-warning')
            }

            if (email.value.length == 0) {
                email.parentElement.classList.add('show-warning')
                email.parentElement.children[email.parentElement.children.length - 1].innerHTML = 'Ingresa tu correo'
                validate = false
            } else if (!patternEmail.test(email.value)) {
                email.parentElement.classList.add('show-warning')
                email.parentElement.children[email.parentElement.children.length - 1].innerHTML = 'Ingresa un correo válido'
                validate = false
            } else {
                email.parentElement.classList.remove('show-warning')
            }
            //Celular
            if (cel.value.length == 0) {
                cel.parentElement.classList.add('show-warning')
                cel.parentElement.children[cel.parentElement.children.length - 1].innerHTML = 'Ingresa tu télefono'
                validate = false
            } else if (!patternCel.test(cel.value)) {
                cel.parentElement.classList.add('show-warning')
                cel.parentElement.children[cel.parentElement.children.length - 1].innerHTML = 'Ingresa un número válido'
                validate = false
            } else {
                cel.parentElement.classList.remove('show-warning')
            }

            return validate
        },

    };

    const initialize = function () {


        if (document.querySelector('body').classList.contains('sign-up')) {
            events.onClickSignUp()
        }
        else {
            events.onClickSent()
        }

    };

    return {
        init: initialize
    };
})();


document.addEventListener(
    'DOMContentLoaded',
    function () {
        webHampik.init();
    },
    false
);